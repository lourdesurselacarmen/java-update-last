package com.springboot.service;

import com.springboot.dto.transaction.TransactionDTO;
import com.springboot.model.groupActivity.GroupTransaction;
import com.springboot.model.groupMaking.Group;
import com.springboot.model.groupMaking.Member;
import com.springboot.model.transaction.GroupWallet;
import com.springboot.model.transaction.Transaction;
import com.springboot.model.transaction.Wallet;
import com.springboot.model.user.User;
import com.springboot.repo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;

@Service
public class TransactionService {
    public static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);

    @Autowired
    private TransactionRepo transactionRepo;
    @Autowired
    private GroupTransactionRepo groupTransactionRepo;
    @Autowired
    private WalletRepo walletRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private GroupWalletRepo groupWalletRepo;
    @Autowired
    private DataUserService dataUserService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private HistoryService historyService;

    public Wallet topUp(double topUp) {
        User currentUser = dataUserService.findCurrentUser();
        Wallet wallets = currentUser.getWallet();
        double nowBalance = wallets.getWalletBalance();
        double afterBalance = nowBalance + topUp;
        wallets.setWalletBalance(afterBalance);
        return walletRepo.save(wallets);
    }

    public Wallet getArisanWalletUser() {
        long idUser = dataUserService.findByPhoneNumber();
        User users = userRepo.findById(idUser).get();
        return users.getWallet();
    }

    public Wallet getArisanWalletUserTarget(String noTelp) {
        long idUser = dataUserService.findByPhoneNumberTarget(noTelp);
        User users = userRepo.findById(idUser).get();
        return users.getWallet();
    }

    public Transaction dataArisan(TransactionDTO transactionInfo, String groupName) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = groupService.findGroupName(groupName);
        if (groups != null) {
            List<Member> members = groups.getMembers();
            for (Member eachMember : members) {
                if (currentUser.getUsername().equals(eachMember.getMemberName())) {
                    Transaction transactions = new Transaction();
                    transactions.setPaymentDeadline(transactionInfo.getPaymentDeadline());
                    transactions.setTransactionValue(groups.getTotalMoney());
                    transactions.setIs_paid(false);
                    transactions.setMember(eachMember);
                    transactions.setDenda(0);
                    transactionRepo.save(transactions);
                    eachMember.addTransaction(transactions);
                    memberRepo.save(eachMember);

                    GroupTransaction groupTransactions = new GroupTransaction();
                    groupTransactions.setMoneyTransfered(groups.getTotalMoney());
                    groupTransactions.setMemberInvolved(currentUser.getUsername());
                    groupTransactions.setTransfered(false);
                    groupTransactions.setWinner("-");
                    groupTransactions.setDescriptionActivity("Mendata deadline arisan");
                    groupTransactions.setGroup(groups);
                    groupTransactions.setTransaction(transactions);
                    groupTransactionRepo.save(groupTransactions);

                    return transactions;
                }
            }
        }
        return null;
    }

    public Transaction payArisanCheck(String groupName, long idGroupTransaction, TransactionDTO transactionInfo) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = groupService.findGroupName(groupName);
        if (groups != null) {
            List<Member> members = groups.getMembers();
            for (Member eachMember : members) {
                if (currentUser.getUsername().equals(eachMember.getMemberName())) {
                    GroupTransaction groupTransactions = groupTransactionRepo.findById(idGroupTransaction).get();
                    Transaction transactions = groupTransactions.getTransaction();
                    if (transactions.isIs_paid()) {
                        return null;
                    } else {
                        if (payArisanWallet(transactions.getTransactionValue(), groupName, transactionInfo.getDenda()) == null) {
                            return null;
                        } else {
                            transactions.setIs_paid(true);
                            transactions.setDenda(transactionInfo.getDenda());
                            transactionRepo.save(transactions);
                            eachMember.addTransaction(transactions);
                            memberRepo.save(eachMember);

                            groupTransactions.setTransfered(true);
                            groupTransactions.setDescriptionActivity("Sudah bayar arisan");
                            groupTransactionRepo.save(groupTransactions);

                            return transactions;
                        }
                    }
                }
            }
        }
        return null;
    }

    public GroupWallet findByGroupName(String groupName) {
        List<GroupWallet> groupWallets = groupWalletRepo.findAll();
        for (GroupWallet eachGroupWallet: groupWallets) {
            Group groups = groupService.findGroupName(groupName);
            long idGroup = groups.getIdGroup();
            if (idGroup == eachGroupWallet.getGroup().getIdGroup()) {
                return eachGroupWallet;
            }
        }
        return null;
    }

    public Wallet payArisanWallet(double totalMoney, String groupName, double denda) {

        Wallet walletUser = getArisanWalletUser();
        double beforeBalance = walletUser.getWalletBalance();
        double nowBalance = beforeBalance - totalMoney - denda;

        if (nowBalance >= 0) {
            walletUser.setWalletBalance(nowBalance);

            GroupWallet walletTarget = findByGroupName(groupName);
            double beforeBalanceTarget = walletTarget.getGroupWalletBalance();
            double nowBalanceTarget = beforeBalanceTarget + totalMoney + denda;

            walletTarget.setGroupWalletBalance(nowBalanceTarget);
            groupWalletRepo.save(walletTarget);
            return walletRepo.save(walletUser);
        }
        return null;
    }

    public Member kocok(List<Member> members) {
        Random rand = new Random();
        int index = rand.nextInt(members.size());
        logger.info("Kocok arisan");
        return members.get(index);
    }

    public Member kocokArisan (String groupName) {
        Group groups = groupService.findGroupName(groupName);
        List<Member> members = groups.getMembers();
        List<GroupTransaction> groupTransactions = groups.getGroupTransactions();
        Member winnerMember = kocok(members);
        boolean flag = true;
        String winner = winnerMember.getMemberName();
        if (groupTransactions.size() > 0) {
            for (GroupTransaction eachGroupTransaction : groupTransactions) {
                logger.info("Pemenang saat ini: " + winner);
                logger.info("Pemenang sebelumnya: " + eachGroupTransaction.getWinner());
                if (eachGroupTransaction.getWinner().equals(winner)) {
                    flag = false;
                    logger.info("Nama pemenang sudah masuk database sebagai pemenang sebelumnya");
                    break;
                } else {
                    logger.info("Pemenang ditentukan");
                    flag = true;
                }
            }
            if (flag) {
                return winnerMember;
            }
            return null;

        } else {
            return winnerMember;
        }
    }

    public GroupTransaction arisanWinner(String groupName) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = groupService.findGroupName(groupName);
        if (groups != null) {
            List<Member> members = groups.getMembers();
            for (Member eachMember : members) {
                if (currentUser.getUsername().equals(eachMember.getMemberName())) {
                    if (eachMember.isRoleMember()) {
                        Member winnerMember = kocokArisan(groupName);
                        if (winnerMember != null) {
                            Transaction transactions = new Transaction();
                            transactions.setPaymentDeadline(LocalDate.now());
                            transactions.setTransactionValue(groups.getTotalMoney()*groups.getTotalMember());
                            transactions.setIs_paid(false);
                            transactions.setMember(eachMember);
                            transactions.setDenda(0);
                            transactionRepo.save(transactions);
                            eachMember.addTransaction(transactions);
                            memberRepo.save(eachMember);

                            GroupTransaction newGroupTransactions = new GroupTransaction();
                            newGroupTransactions.setWinner(winnerMember.getMemberName());
                            newGroupTransactions.setMoneyTransfered(groups.getTotalMoney()*groups.getTotalMember());
                            newGroupTransactions.setTransfered(false);
                            newGroupTransactions.setDescriptionActivity("Mendata pemenang arisan");
                            newGroupTransactions.setMemberInvolved(currentUser.getUsername());
                            newGroupTransactions.setGroup(groups);
                            newGroupTransactions.setTransaction(transactions);
                            return groupTransactionRepo.save(newGroupTransactions);
                        }
                    }
                }
            }
        }
        return null;
    }

    public GroupTransaction arisanWinnerCheck(String phoneNumber, String groupName, long idTransaction) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = groupService.findGroupName(groupName);
        if (groups != null) {
            List<Member> members = groups.getMembers();
            for (Member eachMember : members) {
                if (currentUser.getUsername().equals(eachMember.getMemberName())) {
                    if (eachMember.isRoleMember()) {
                        GroupTransaction groupTransactions = groupTransactionRepo.findById(idTransaction).get();
                        Transaction transactions = groupTransactions.getTransaction();
                        if (groupTransactions.isTransfered()) {
                            return null;
                        } else {
                            transactions.setIs_paid(true);
                            transactionRepo.save(transactions);
                            eachMember.addTransaction(transactions);
                            memberRepo.save(eachMember);

                            groupTransactions.setTransfered(true);
                            groupTransactions.setDescriptionActivity("Sudah transfer ke pemenang");
                            groupTransactionRepo.save(groupTransactions);
                            GroupWallet groupWallet = payArisanGroupWallet(phoneNumber, groupName);
                            if (groupWallet == null) {
                                return null;
                            } else {
                                return groupTransactions;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public GroupWallet payArisanGroupWallet(String noTelp, String groupName) {
        Group groups = groupService.findGroupName(groupName);
        GroupWallet walletTarget = findByGroupName(groupName);
        double beforeBalanceTarget = walletTarget.getGroupWalletBalance();
        double nowBalanceTarget = beforeBalanceTarget - (groups.getTotalMoney()*groups.getTotalMember());

        if (nowBalanceTarget >= 0) {
            walletTarget.setGroupWalletBalance(nowBalanceTarget);
            groupWalletRepo.save(walletTarget);

            Wallet walletUser = getArisanWalletUserTarget(noTelp);
            double beforeBalance = walletUser.getWalletBalance();
            double afterBalance = groups.getTotalMoney()*groups.getTotalMember();
            double nowBalance = beforeBalance + afterBalance;

            walletUser.setWalletBalance(nowBalance);
            walletRepo.save(walletUser);

            return groupWalletRepo.save(walletTarget);
        }
        return null;
    }

    public GroupWallet showSaldoGroupWallet(String groupName) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = groupService.findGroupName(groupName);
        if (groups != null) {
            long idGroup = groups.getIdGroup();
            List<Member> members = groups.getMembers();
            for (Member eachMember : members) {
                if (currentUser.getUsername().equals(eachMember.getMemberName())) {
                    List<GroupWallet> groupWallets = groupWalletRepo.findAll();
                    for (GroupWallet eachGroupWallet : groupWallets) {
                        if (idGroup == eachGroupWallet.getGroup().getIdGroup()) {
                            return eachGroupWallet;
                        }
                    }
                }
            }
        }
        return null;
    }

    public GroupWallet showSaldoGroupWalletAdmin(String groupName) {
        Group groups = groupService.findGroupName(groupName);
        if (groups != null) {
            long idGroup = groups.getIdGroup();
            List<GroupWallet> groupWallets = groupWalletRepo.findAll();
            for (GroupWallet eachGroupWallet : groupWallets) {
                if (idGroup == eachGroupWallet.getGroup().getIdGroup()) {
                    return eachGroupWallet;
                }
            }
        }
        return null;
    }

    public double getAllTransactionValue() {
        double totalTransaksi = 0.0;
        List<GroupTransaction> groupTransactions = historyService.allHistoryTransactionGroup();
        for (GroupTransaction eachGroupTransaction: groupTransactions) {
            if (eachGroupTransaction.getWinner().equals("-")) {
                double transaction = eachGroupTransaction.getMoneyTransfered();
                totalTransaksi += transaction;
                logger.info("Total Transaksi : " + totalTransaksi);
            }
        }

        return totalTransaksi;
    }

    public double getRevenue() {
        double revenue = 0.0;
        List<GroupTransaction> groupTransactions = historyService.allHistoryTransactionGroup();
        for (GroupTransaction eachGroupTransaction: groupTransactions) {
            if (!eachGroupTransaction.getWinner().equals("-")) {
                double transaction = eachGroupTransaction.getMoneyTransfered()*0.01;
                revenue += transaction;
                logger.info("Revenue : " + revenue);
            }
        }

        return revenue;
    }
}

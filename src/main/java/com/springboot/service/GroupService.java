package com.springboot.service;

import com.springboot.dto.groupActivity.GroupMessageDTO;
import com.springboot.dto.groupMaking.GroupDTO;
import com.springboot.dto.groupMaking.MemberDTO;
import com.springboot.model.groupActivity.GroupMessage;
import com.springboot.model.groupMaking.Group;
import com.springboot.model.groupMaking.Member;
import com.springboot.model.transaction.GroupWallet;
import com.springboot.model.user.User;
import com.springboot.repo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupService {
    public static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private GroupRepo groupRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private GroupMessageRepo groupMessageRepo;
    @Autowired
    private GroupWalletRepo groupWalletRepo;
    @Autowired
    private DataUserService dataUserService;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    public Group createGroup(GroupDTO groupInfo, double money) {
        Group groups = new Group();
        groups.setGroupName(groupInfo.getGroupName());
        groups.setPasscode(bcryptEncoder.encode(groupInfo.getPasscode()));
        groups.setTotalMoney(money);
        groupRepo.save(groups);

        GroupWallet groupWallets = new GroupWallet();
        groupWallets.setGroupWalletBalance(0);
        groupWallets.setGroup(groups);
        GroupWallet groupWalletUpdated = groupWalletRepo.save(groupWallets);

        Group groupUpdated = groupRepo.findById(groups.getIdGroup()).get();
        groupUpdated.setGroupWallet(groupWalletUpdated);
        return groupRepo.save(groupUpdated);
    }

    public Group saveGroup(GroupDTO groupInfo) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = new Group();
        groups.setGroupName(groupInfo.getGroupName());
        groups.setTotalMoney(groupInfo.getTotalMoney());
        groups.setTotalMember(1);
        groups.setPasscode(bcryptEncoder.encode(groupInfo.getPasscode()));

        Member dataMember = new Member();
        dataMember.setMemberName(currentUser.getUsername());
        dataMember.setRoleMember(true);
        dataMember.setStatus("Active");
        groups.addMember(dataMember);

        currentUser.addMember(dataMember);
        groupRepo.save(groups);

        GroupWallet groupWallets = new GroupWallet();
        groupWallets.setGroupWalletBalance(0);
        groupWallets.setGroup(groups);
        GroupWallet groupWalletUpdated = groupWalletRepo.save(groupWallets);

        Group groupUpdated = groupRepo.findById(groups.getIdGroup()).get();
        groupUpdated.setGroupWallet(groupWalletUpdated);
        return groupRepo.save(groupUpdated);
    }

    public Group findGroupName(String groupName) {
        List<Group> groups = groupRepo.findAll();
        for (Group eachGroup: groups) {
            if(groupName.equals(eachGroup.getGroupName())){
                return eachGroup;
            }
        }
        return null;
    }

    public boolean alreadyExistUsername(String groupName) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = findGroupName(groupName);
        List<Member> members = groups.getMembers();
        for (Member eachMembers: members) {
            if(currentUser.getUsername().equals(eachMembers.getMemberName())) {
                return false;
            }
        }
        return true;
    }

    public Group joinGroupMember(GroupDTO groupJoin, String groupName) throws NullPointerException {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = findGroupName(groupName);
        if (groups.getTotalMember()<12) {
            groups.setTotalMember(groups.getTotalMember() + 1);
            PasswordEncoder passencoder = new BCryptPasswordEncoder();
            String encodedPassword = groups.getPasscode();

            if (passencoder.matches(groupJoin.getPasscode(), encodedPassword)) {
                if (!alreadyExistUsername(groupName)) {
                    return null;
                } else {
                    Member dataMember = new Member();
                    dataMember.setMemberName(currentUser.getUsername());
                    dataMember.setRoleMember(false);
                    dataMember.setStatus("Active");
                    dataMember.setUser(userRepo.findByUsername(currentUser.getUsername()));
                    groups.addMember(dataMember);
                    return groupRepo.save(groups);
                }
            }
        }

        return null;
    }

    public List<Group> findAllGroup() {
        return groupRepo.findAll();
    }

    public List<Member> findMemberbyUsername() {
        User currentUser = dataUserService.findCurrentUser();
        List<Member> members = memberRepo.findByMemberName(currentUser.getUsername());
        return members;
    }

    public List<Member> findMemberbyUsernameAdmin(String username) {
        List<Member> members = memberRepo.findByMemberName(username);
        return members;
    }

    public List<Group> listGroup(List<Member> members) {
        List<Group> groupLists = new ArrayList<>();
        for (Member eachMember: members) {
            Group groups = groupRepo.findById_group(eachMember.getGroup().getIdGroup());
            groupLists.add(groups);
        }
        return groupLists;
    }

    public Member findMemberbyUsernameInGroup(String groupName) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = findGroupName(groupName);
        List<Member> members = groups.getMembers();
        for (Member eachMember: members) {
            if (currentUser.getUsername().equals(eachMember.getMemberName())) {
                return eachMember;
            }
        }

        return null;
    }

    public Group changeGroupInfo(GroupDTO groupInfo, String groupName) {
        Group groups = findGroupName(groupName);

            Member findByUsername = findMemberbyUsernameInGroup(groupName);
            if (findByUsername != null) {
                if (findByUsername.isRoleMember()) {
                    groups.setGroupName(groupInfo.getGroupName());
                    if (!groupInfo.getPasscode().equals("")) {
                        groups.setPasscode(bcryptEncoder.encode(groupInfo.getPasscode()));
                    } else {
                        groups.setPasscode(groups.getPasscode());
                    }
                    return groupRepo.save(groups);
                }
            }
        return null;
    }

    public Group changeAdmin(String groupName, MemberDTO memberInfo) {
        Group groups = findGroupName(groupName);

        List<Member> members = groups.getMembers();
        for (Member eachMember: members) {
            eachMember.setRoleMember(false);
            if(eachMember.getMemberName().equals(memberInfo.getMemberName())) {
                eachMember.setRoleMember(true);
                return groupRepo.save(groups);
            }
        }
        return null;
    }

    public List<Member> findMemberGroup(String groupName) {
        Group groups = findGroupName(groupName);
        if (groups == null) {
            return null;
        } else {
            return groups.getMembers();
        }
    }

    public boolean checkGroupAdmin(String groupName) {
        User currentUser = dataUserService.findCurrentUser();
        List<Member> members = findMemberGroup(groupName);
        for (Member eachMember: members) {
            if (eachMember.getMemberName().equals(currentUser.getUsername())) {
                if (eachMember.isRoleMember()) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<GroupMessage> postMessage(String groupName, GroupMessageDTO groupMessageFill) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = findGroupName(groupName);
        Member members = findMemberbyUsernameInGroup(groupName);
        if (members == null) {
            return null;
        } else {
            GroupMessage groupMessages = new GroupMessage();
            groupMessages.setMemberName(currentUser.getUsername());
            groupMessages.setMessage(groupMessageFill.getMessage());
            groupMessages.setMember(findMemberbyUsernameInGroup(groupName));
            groups.addGroupMessages(groupMessages);
            groupMessageRepo.save(groupMessages);

            return groups.getGroupMessages();
        }
    }

    public List<GroupMessage> getMessages(String groupName) {
        Group groups = findGroupName(groupName);
        Member members = findMemberbyUsernameInGroup(groupName);
        if (members == null) {
            return null;
        } else {
            return groups.getGroupMessages();
        }
    }

    public List<GroupMessage> deleteMessage(String groupName, long idMessage) {
        User currentUser = dataUserService.findCurrentUser();
        Group groups = findGroupName(groupName);
        Member members = findMemberbyUsernameInGroup(groupName);
        if (members != null) {
            List<GroupMessage> groupMessages = groups.getGroupMessages();
            for (GroupMessage eachGroupMessage : groupMessages) {
                if (idMessage == eachGroupMessage.getIdMessage()) {
                    String usernameMessage = eachGroupMessage.getMemberName();
                    if (currentUser.getUsername().equals(usernameMessage)) {
                        GroupMessage messageToBeDeleted = groupMessageRepo.findById(idMessage).get();
                        groups.removeGroupMessages(messageToBeDeleted);
                        members.removeGroupMessage(messageToBeDeleted);
                        groupMessageRepo.delete(messageToBeDeleted);
                        return groups.getGroupMessages();
                    }
                }
            }
        }
        return null;
    }
}

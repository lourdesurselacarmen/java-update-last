package com.springboot.service;

import com.springboot.dto.user.UserDTO;
import com.springboot.model.user.User;
import com.springboot.repo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DataUserService {

    public static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private GroupRepo groupRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private PasswordEncoder bcryptEncoder;

    public List<User> findAllData() {
        return userRepo.findAll();
    }

    public User findByUserId(long id) {
        try {
            User users = userRepo.findById(id).get();
            return users;
        } catch (Exception e) {
            logger.error(String.valueOf(e));
            return null;
        }
    }

    public User findByUsername(String username) {
        try {
            User users = userRepo.findByUsername(username);
            return users;
        } catch (Exception e) {
            logger.error(String.valueOf(e));
            return null;
        }
    }

    public long findByPhoneNumber() {
        User currentUser = findCurrentUser();
        List<User> users = userRepo.findAll();
        for (User eachUser: users) {
            if(currentUser.getPhoneNumber().equals(eachUser.getPhoneNumber())){
                return eachUser.getIdUser();
            }
        }
        return 0;
    }

    public long findByPhoneNumberTarget(String noTelp) {
        List<User> users = userRepo.findAll();
        for (User eachUser: users) {
            if(noTelp.equals(eachUser.getPhoneNumber())){
                return eachUser.getIdUser();
            }
        }
        return 0;
    }

    public User changeProfile(UserDTO userUpdate) {
        User updatedUser = findCurrentUser();
        updatedUser.setFullName(userUpdate.getFullName());
        updatedUser.setPhoneNumber(userUpdate.getPhoneNumber());
        updatedUser.setDateOfBirth(userUpdate.getDateOfBirth());
        updatedUser.setGender(userUpdate.getGender());
        if (!userUpdate.getPassword().equals("")) {
            updatedUser.setPassword(bcryptEncoder.encode(userUpdate.getPassword()));
        } else {
            updatedUser.setPassword(updatedUser.getPassword());
        }
        return userRepo.save(updatedUser);
    }

    public User changeProfileAdmin(UserDTO userUpdate) {
        User updatedUser = findCurrentUser();
        updatedUser.setFullName(userUpdate.getFullName());
        updatedUser.setDateOfBirth(userUpdate.getDateOfBirth());
        updatedUser.setGender(userUpdate.getGender());
        return userRepo.save(updatedUser);
    }

    public User findCurrentUser() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = userDetails.getUsername();
        return userRepo.findByUsername(username);
    }
}

package com.springboot.service;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class UploadFileService {
    private String uploadDirectory = System.getProperty("user.dir") + "/uploads/";

    public String uploadFile(MultipartFile file) throws IllegalStateException, IOException {
        file.transferTo(new File(uploadDirectory + file.getOriginalFilename()));

        return file.getOriginalFilename();
    }

    Path rootLocation = Paths.get(uploadDirectory);

    public Resource load(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
}

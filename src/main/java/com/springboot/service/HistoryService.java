package com.springboot.service;

import com.springboot.model.groupActivity.GroupTransaction;
import com.springboot.model.groupMaking.Group;
import com.springboot.model.groupMaking.Member;
import com.springboot.model.transaction.Transaction;
import com.springboot.model.user.User;
import com.springboot.repo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HistoryService {

    public static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private GroupRepo groupRepo;
    @Autowired
    private GroupMessageRepo groupMessageRepo;
    @Autowired
    private GroupTransactionRepo groupTransactionRepo;
    @Autowired
    private TransactionRepo transactionRepo;
    @Autowired
    private MemberRepo memberRepo;
    @Autowired
    private WalletRepo walletRepo;
    @Autowired
    private DataUserService dataUserService;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    public List<Transaction> allHistoryTransactionUser() {
        return transactionRepo.findAll();
    }
    
    public List<List<Transaction>> historyTransactionUser() {
        User currentUser = dataUserService.findCurrentUser();
        logger.info(currentUser.getUsername());
        List<Member> members = memberRepo.findAll();
        List<List<Transaction>> transactions = new ArrayList<>();
        List<Transaction> transactions1 = new ArrayList<>();
        for (Member eachMember: members) {
            if (currentUser.getUsername().equals(eachMember.getMemberName())) {
                transactions1 = eachMember.getTransactions();
                transactions.add(transactions1);
            }
        }
        return transactions;
    }

    public List<List<Transaction>> historyTransactionUserAdmin(String username) {
        logger.info(username);
        List<Member> members = memberRepo.findAll();
        List<List<Transaction>> transactions = new ArrayList<>();
        List<Transaction> transactions1 = new ArrayList<>();
        for (Member eachMember: members) {
            if (username.equals(eachMember.getMemberName())) {
                transactions1 = eachMember.getTransactions();
                transactions.add(transactions1);
            }
        }
        return transactions;
    }

    public List<GroupTransaction> allHistoryTransactionGroup() {
        return groupTransactionRepo.findAll();
    }

    public List<GroupTransaction> historyTransactionGroup(String groupName) {
        List<Group> groups = groupRepo.findAll();
        for (Group eachGroup: groups) {
            if (groupName.equals(eachGroup.getGroupName())) {
                return eachGroup.getGroupTransactions();
            }
        }
        return null;
    }
}

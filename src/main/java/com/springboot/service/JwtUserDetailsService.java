package com.springboot.service;

import com.springboot.model.transaction.Wallet;
import com.springboot.model.user.User;
import com.springboot.dto.user.UserDTO;
import com.springboot.repo.UserRepo;
import com.springboot.repo.WalletRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JwtUserDetailsService implements UserDetailsService {

        public static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private WalletRepo walletRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("Creating Product : {}", username);
        User user = userRepo.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
    }

    public User findByUsername(String username) {
        User user = userRepo.findByUsername(username);
        return user;
    }

    public User save(UserDTO user) {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setFullName(user.getFullName());
        newUser.setPhoneNumber(user.getPhoneNumber());
        newUser.setDateOfBirth(user.getDateOfBirth());
        newUser.setGender(user.getGender());
        newUser.setAdmin(false);
        userRepo.save(newUser);

        User userUpdated = userRepo.findByUsername(user.getUsername());

        Wallet newWallet = new Wallet();
        newWallet.setWalletBalance(10000000);
        newWallet.setUser(userUpdated);
        Wallet walletUpdated = walletRepo.save(newWallet);

        userUpdated.setWallet(walletUpdated);

        return userRepo.save(userUpdated);
    }

    public User saveAdmin(UserDTO user) {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setFullName(user.getFullName());
        newUser.setDateOfBirth(user.getDateOfBirth());
        newUser.setGender(user.getGender());
        newUser.setAdmin(true);

        return userRepo.save(newUser);
    }

    public boolean findUsername(String username) {
        List<User> listUser = userRepo.findAll();
        for (User eachUser: listUser) {
            if(username.equals(eachUser.getUsername())) {
                return true;
            }
        }
        return false;
    }

    public boolean findPhone(String phone) {
        List<User> listUser = userRepo.findAll();
        for (User eachUser: listUser) {
            if(phone.equals(eachUser.getPhoneNumber())) {
                return true;
            }
        }
        return false;
    }
}
package com.springboot.dto.groupMaking;

import com.springboot.model.groupActivity.GroupMessage;
import com.springboot.model.groupMaking.Group;
import com.springboot.model.transaction.Transaction;
import com.springboot.model.user.User;

import java.util.ArrayList;
import java.util.List;

public class MemberDTO {
    private String memberName;
    private boolean roleMember;
    private String status;
    private User user;
    private Group group;
    private List<Transaction> transactions = new ArrayList<>();
    private List<GroupMessage> groupMessages = new ArrayList<>();

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean getRoleMember() {
        return roleMember;
    }

    public void setRoleMember(boolean roleMember) {
        this.roleMember = roleMember;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public List<GroupMessage> getGroupMessages() {
        return groupMessages;
    }

    public void setGroupMessages(List<GroupMessage> groupMessages) {
        this.groupMessages = groupMessages;
    }
}
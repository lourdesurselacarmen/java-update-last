package com.springboot.dto.groupMaking;

import com.springboot.model.groupActivity.GroupMessage;
import com.springboot.model.groupActivity.GroupTransaction;
import com.springboot.model.groupMaking.Member;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GroupDTO {
    private String groupName;
    private int totalMember;
    private LocalDate createdDate = LocalDate.now();
    private double totalMoney;
    private String passcode;
    private List<Member> members = new ArrayList<>();
    private List<GroupTransaction> groupTransactions = new ArrayList<>();
    private List<GroupMessage> groupMessages = new ArrayList<>();

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getTotalMember() {
        return totalMember;
    }

    public void setTotalMember(int totalMember) {
        this.totalMember = totalMember;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public List<GroupTransaction> getGroupTransactions() {
        return groupTransactions;
    }

    public void setGroupTransactions(List<GroupTransaction> groupTransactions) {
        this.groupTransactions = groupTransactions;
    }

    public List<GroupMessage> getGroupMessages() {
        return groupMessages;
    }

    public void setGroupMessages(List<GroupMessage> groupMessages) {
        this.groupMessages = groupMessages;
    }
}
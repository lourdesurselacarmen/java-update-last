package com.springboot.dto.groupActivity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

public class GroupMessageDTO {
    private String memberName;
    private String message;
    private LocalDate createdDate = LocalDate.now();
    private LocalTime createdTime = LocalTime.now();

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalTime createdTime) {
        this.createdTime = createdTime;
    }
}
package com.springboot.dto.groupActivity;

import java.time.LocalDate;

public class GroupTransactionDTO {
    private String winner;
    private boolean isTransfered;
    private double moneyTransfered;
    private LocalDate dateOfGroupTransaction;

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public boolean isTransfered() {
        return isTransfered;
    }

    public void setTransfered(boolean transfered) {
        isTransfered = transfered;
    }

    public double getMoneyTransfered() {
        return moneyTransfered;
    }

    public void setMoneyTransfered(double moneyTransfered) {
        this.moneyTransfered = moneyTransfered;
    }

    public LocalDate getDateOfGroupTransaction() {
        return dateOfGroupTransaction;
    }

    public void setDateOfGroupTransaction(LocalDate dateOfGroupTransaction) {
        this.dateOfGroupTransaction = dateOfGroupTransaction;
    }
}
package com.springboot.dto.transaction;

import com.springboot.model.user.User;

public class WalletDTO {
    private double walletBalance;
    private User user;

    public double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(double walletBalance) {
        this.walletBalance = walletBalance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
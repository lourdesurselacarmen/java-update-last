package com.springboot.dto.transaction;

public class GroupWalletDTO {

    private long idGroupWallet;
    private double groupWalletBalance;

    public long getIdGroupWallet() {
        return idGroupWallet;
    }

    public void setIdGroupWallet(long idGroupWallet) {
        this.idGroupWallet = idGroupWallet;
    }

    public double getGroupWalletBalance() {
        return groupWalletBalance;
    }

    public void setGroupWalletBalance(double groupWalletBalance) {
        this.groupWalletBalance = groupWalletBalance;
    }
}

package com.springboot.model.groupMaking;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.model.transaction.Transaction;
import com.springboot.model.user.User;
import com.springboot.model.groupActivity.GroupMessage;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "member")
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idMember;
    @Column(nullable = false)
    private String memberName;
    @Column
    private String status;
    @Column
    private boolean roleMember;

    public long getIdMember() {
        return idMember;
    }

    public void setIdMember(long idMember) {
        this.idMember = idMember;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isRoleMember() {
        return roleMember;
    }

    public void setRoleMember(boolean roleMember) {
        this.roleMember = roleMember;
    }

    public Member() {}

    @ManyToOne
    @JoinColumn(name="id_user")
    @JsonIgnoreProperties("member")
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @ManyToOne
    @JoinColumn(name="id_group")
    @JsonIgnore
    private Group group;

    public void setGroup(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("member")
    private List<Transaction> transactions = new ArrayList<>();

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
        transaction.setMember(this);
    }

    public void removeTransaction(Transaction transaction) {
        transactions.remove(transaction);
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("member")
    private List<GroupMessage> groupMessages = new ArrayList<>();

    public void addGroupMessage(GroupMessage groupMessage) {
        groupMessages.add(groupMessage);
        groupMessage.setMember(this);
    }

    public void removeGroupMessage(GroupMessage groupMessage) {
        groupMessages.remove(groupMessage);
    }

    public List<GroupMessage> getGroupMessages() {
        return groupMessages;
    }

    public void setGroupMessages(List<GroupMessage> groupMessages) {
        this.groupMessages = groupMessages;
    }
}
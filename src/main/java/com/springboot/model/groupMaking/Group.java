package com.springboot.model.groupMaking;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.model.groupActivity.GroupMessage;
import com.springboot.model.groupActivity.GroupTransaction;
import com.springboot.model.transaction.GroupWallet;
import com.springboot.model.transaction.Wallet;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "group_list")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idGroup;
    @Column(nullable = false, unique = true)
    private String groupName;
    @Column(nullable = false)
    private int totalMember;
    @Column
    private LocalDate createdDate = LocalDate.now();
    @Column(nullable = false)
    private double totalMoney;
    @Column(nullable = false)
    @JsonIgnore
    private String passcode;

    public long getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(long idGroup) {
        this.idGroup = idGroup;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getTotalMember() {
        return totalMember;
    }

    public void setTotalMember(int totalMember) {
        this.totalMember = totalMember;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public Group() {}

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("group")
    private List<Member> members = new ArrayList<>();

    public void addMember(Member member) {
        members.add(member);
        member.setGroup(this);
    }

    public void removeMember(Member member) {
        members.remove(member);
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("group")
    private List<GroupTransaction> groupTransactions = new ArrayList<>();

    public void addGroupTransaction(GroupTransaction groupTransaction) {
        groupTransactions.add(groupTransaction);
        groupTransaction.setGroup(this);
    }

    public void removeGroupTransaction(GroupTransaction groupTransaction) {
        groupTransactions.remove(groupTransaction);
    }

    public List<GroupTransaction> getGroupTransactions() {
        return groupTransactions;
    }

    public void setGroupTransactions(List<GroupTransaction> groupTransactions) {
        this.groupTransactions = groupTransactions;
    }

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("group")
    private List<GroupMessage> groupMessages = new ArrayList<>();

    public void addGroupMessages(GroupMessage groupMessage) {
        groupMessages.add(groupMessage);
        groupMessage.setGroup(this);
    }

    public void removeGroupMessages(GroupMessage groupMessage) {
        groupMessages.remove(groupMessage);
    }

    public List<GroupMessage> getGroupMessages() {
        return groupMessages;
    }

    public void setGroupMessages(List<GroupMessage> groupMessages) {
        this.groupMessages = groupMessages;
    }

    @OneToOne(mappedBy = "group")
    @JsonIgnoreProperties("group")
    private GroupWallet groupWallet;

    public GroupWallet getGroupWallet() {
        return groupWallet;
    }

    public void setGroupWallet(GroupWallet groupWallet) {
        this.groupWallet = groupWallet;
    }
}
package com.springboot.model.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.model.groupMaking.Group;
import com.springboot.model.user.User;

import javax.persistence.*;

@Entity
@Table(name = "group_wallet")
public class GroupWallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idGroupWallet;
    @Column(nullable = false)
    private double groupWalletBalance;

    public long getIdGroupWallet() {
        return idGroupWallet;
    }

    public void setIdGroupWallet(long idGroupWallet) {
        this.idGroupWallet = idGroupWallet;
    }

    public double getGroupWalletBalance() {
        return groupWalletBalance;
    }

    public void setGroupWalletBalance(double groupWalletBalance) {
        this.groupWalletBalance = groupWalletBalance;
    }

    public GroupWallet() {}

    @OneToOne
    @JoinColumn(name = "id_group")
    @JsonIgnore
    private Group group;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }


}

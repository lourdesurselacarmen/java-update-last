package com.springboot.model.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.model.user.User;

import javax.persistence.*;

@Entity
@Table(name = "wallet")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idWallet;
    @Column(nullable = false)
    private double walletBalance;

    public long getIdWallet() {
        return idWallet;
    }

    public void setIdWallet(long idWallet) {
        this.idWallet = idWallet;
    }

    public double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(double walletBalance) {
        this.walletBalance = walletBalance;
    }

    public Wallet() {}

    @OneToOne
    @JoinColumn(name = "id_user")
    @JsonIgnoreProperties("wallet")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
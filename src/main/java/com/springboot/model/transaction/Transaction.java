package com.springboot.model.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.model.groupActivity.GroupTransaction;
import com.springboot.model.groupMaking.Member;
import com.springboot.model.user.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idTransaction;
    @Column(nullable = false)
    private LocalDate paymentDeadline;
    @Column(nullable = false)
    private double transactionValue;
    @Column
    private LocalDate dateOfTransaction = LocalDate.now();
    @Column
    private  double denda;
    @Column(nullable = false)
    private boolean is_paid;

    public long getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(long idTransaction) {
        this.idTransaction = idTransaction;
    }

    public LocalDate getPaymentDeadline() {
        return paymentDeadline;
    }

    public void setPaymentDeadline(LocalDate paymentDeadline) {
        this.paymentDeadline = paymentDeadline;
    }

    public double getTransactionValue() {
        return transactionValue;
    }

    public void setTransactionValue(double transactionValue) {
        this.transactionValue = transactionValue;
    }

    public LocalDate getDateOfTransaction() {
        return dateOfTransaction;
    }

    public void setDateOfTransaction(LocalDate dateOfTransaction) {
        this.dateOfTransaction = dateOfTransaction;
    }

    public double getDenda() {
        return denda;
    }

    public void setDenda(double denda) {
        this.denda = denda;
    }

    public boolean isIs_paid() {
        return is_paid;
    }

    public void setIs_paid(boolean is_paid) {
        this.is_paid = is_paid;
    }

    public Transaction() {}

    @ManyToOne
    @JoinColumn(name="id_member")
    @JsonIgnoreProperties("transaction")
    private Member member;

    public void setMember(Member member) {
        this.member = member;
    }

    @OneToOne(mappedBy = "transaction")
    @JsonIgnoreProperties("transaction")
    private GroupTransaction groupTransaction;

    public GroupTransaction getGroupTransaction() {
        return groupTransaction;
    }

    public void setGroupTransaction(GroupTransaction groupTransaction) {
        this.groupTransaction = groupTransaction;
    }
}
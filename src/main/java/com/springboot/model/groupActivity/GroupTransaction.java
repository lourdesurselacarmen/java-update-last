package com.springboot.model.groupActivity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.model.groupMaking.Group;
import com.springboot.model.transaction.Transaction;
import com.springboot.model.transaction.Wallet;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "group_transaction")
public class GroupTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idGroupTransaction;
    @Column(nullable = false)
    private String memberInvolved;
    @Column
    private String winner;
    @Column(nullable = false)
    private String descriptionActivity;
    @Column(nullable = false)
    private boolean isTransfered;
    @Column(nullable = false)
    private double moneyTransfered;
    @Column(nullable = false)
    private LocalDate dateOfGroupTransaction = LocalDate.now();

    public long getIdGroupTransaction() {
        return idGroupTransaction;
    }

    public void setIdGroupTransaction(long idGroupTransaction) {
        this.idGroupTransaction = idGroupTransaction;
    }

    public String getMemberInvolved() {
        return memberInvolved;
    }

    public void setMemberInvolved(String memberInvolved) {
        this.memberInvolved = memberInvolved;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getDescriptionActivity() {
        return descriptionActivity;
    }

    public void setDescriptionActivity(String descriptionActivity) {
        this.descriptionActivity = descriptionActivity;
    }

    public boolean isTransfered() {
        return isTransfered;
    }

    public void setTransfered(boolean transfered) {
        isTransfered = transfered;
    }

    public double getMoneyTransfered() {
        return moneyTransfered;
    }

    public void setMoneyTransfered(double moneyTransfered) {
        this.moneyTransfered = moneyTransfered;
    }

    public LocalDate getDateOfGroupTransaction() {
        return dateOfGroupTransaction;
    }

    public void setDateOfGroupTransaction(LocalDate dateOfGroupTransaction) {
        this.dateOfGroupTransaction = dateOfGroupTransaction;
    }

    public GroupTransaction() {}

    @ManyToOne
    @JoinColumn(name="id_group")
    @JsonIgnoreProperties("group_transaction")
    private Group group;

    public void setGroup(Group group) {
        this.group = group;
    }

    @OneToOne
    @JoinColumn(name = "id_transaction")
    @JsonIgnoreProperties("group_transaction")
    private Transaction transaction;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
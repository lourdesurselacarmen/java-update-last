package com.springboot.model.groupActivity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.springboot.model.groupMaking.Group;
import com.springboot.model.groupMaking.Member;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Table(name = "group_message")
public class GroupMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idMessage;
    @Column(nullable = false)
    private String memberName;
    @Column
    private String message;
    @Column
    private LocalDate createdDate = LocalDate.now();
    @Column
    private LocalTime createdTime = LocalTime.now();

    public long getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(long idMessage) {
        this.idMessage = idMessage;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalTime createdTime) {
        this.createdTime = createdTime;
    }

    public GroupMessage() {}

    @ManyToOne
    @JoinColumn(name="id_member")
    @JsonIgnoreProperties("group_message")
    private Member member;

    public void setMember(Member member) {
        this.member = member;
    }

    @ManyToOne
    @JoinColumn(name="id_group")
    @JsonIgnoreProperties("group_message")
    private Group group;

    public void setGroup(Group group) {
        this.group = group;
    }
}
package com.springboot.repo;

import com.springboot.model.groupMaking.Group;
import com.springboot.model.groupMaking.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberRepo extends JpaRepository<Member, Long> {
    List<Member> findByMemberName(String memberName);
}
package com.springboot.repo;

import com.springboot.model.groupActivity.GroupMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupMessageRepo extends JpaRepository<GroupMessage, Long> {
}
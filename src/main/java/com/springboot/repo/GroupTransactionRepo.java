package com.springboot.repo;

import com.springboot.model.groupActivity.GroupTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupTransactionRepo extends JpaRepository<GroupTransaction, Long> {
}
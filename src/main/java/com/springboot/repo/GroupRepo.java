package com.springboot.repo;

import com.springboot.model.groupMaking.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepo extends JpaRepository<Group, Long> {
    @Query(value = "select * from group_list where id_group = ?1", nativeQuery = true)
    Group findById_group(long id_group);
}
package com.springboot.repo;

import com.springboot.model.groupMaking.Member;
import com.springboot.model.transaction.GroupWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupWalletRepo extends JpaRepository<GroupWallet, Long> {
}
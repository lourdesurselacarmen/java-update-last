package com.springboot.controller;

import com.springboot.dto.transaction.TransactionDTO;
import com.springboot.dto.transaction.WalletDTO;
import com.springboot.model.groupActivity.GroupTransaction;
import com.springboot.model.transaction.GroupWallet;
import com.springboot.model.transaction.Transaction;
import com.springboot.model.transaction.Wallet;
import com.springboot.response.ResponseHelper;
import com.springboot.response.ResponseHelperError;
import com.springboot.service.DataUserService;
import com.springboot.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/arisan-arisan-club")
@CrossOrigin(origins="*")
public class TransactionController {
    public static final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private TransactionService transactionService;
    @Autowired
    private DataUserService dataUserService;

    // ------------------------------------------TopUp Dompet Dummy-------------------------------------------------

    @RequestMapping(value = "/topUp", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> topUpWallet(@RequestBody WalletDTO walletInfo) {
        logger.info("[Top up wallet user]");

        try {
            Wallet wallets = transactionService.topUp(walletInfo.getWalletBalance());
            if (wallets == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Top up gagal", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Top up berhasil", wallets), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari top up wallet user: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------Data Arisan Money-------------------------------------------------

    @RequestMapping(value = "/payment-arisan-data/{groupName}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> transactionPaymentUserData(@PathVariable("groupName") String groupName, @RequestBody TransactionDTO transactionInfo) {
        logger.info("[Mendata deadline transaksi arisan]");

        try {
            Transaction transactions = transactionService.dataArisan(transactionInfo, groupName);
            if (transactions == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Kamu bukan member grup ini atau data yang kamu inputkan salah", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Mendata transaksi dalam grup arisan", transactions), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari mendata transaksi user dalam grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------Pay Arisan Money-------------------------------------------------

    @RequestMapping(value = "/paymentCheck-arisan-data/{groupName}/{idGroupTransaksi}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> transactionPaymentUserDataCheck(@PathVariable("groupName") String groupName, @PathVariable("idGroupTransaksi") long idGroupTransaksi, @RequestBody TransactionDTO transactionDTO) {
        logger.info("[Membayar arisan oleh user]");

        try {
            Transaction transactions = transactionService.payArisanCheck(groupName, idGroupTransaksi, transactionDTO);
            if (transactions == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Pembayaran gagal, silahkan cek saldo anda", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Pembayaran berhasil, silahkan cek saldo wallet anda", transactions), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari membayar arisan user: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------Data Group Transaction-------------------------------------------------

    @RequestMapping(value = "/kocok-arisan/{groupName}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> walletBersama(@PathVariable("groupName") String groupName) {
        logger.info("[Mendata transaksi grup dalam grup " + groupName + "]");

        try {
            GroupTransaction groupTransactions = transactionService.arisanWinner(groupName);
                if (groupTransactions == null) {
                    return new ResponseEntity<>(new ResponseHelperError(false, "Kocok arisan gagal, anda bukan seorang admin atau pemenang sudah pernah menang sebelumnya", 400), HttpStatus.BAD_REQUEST);
                } else {
                    return new ResponseEntity<>(new ResponseHelper(true, "Mendata transaksi grup", groupTransactions), HttpStatus.OK);
                }
        } catch (Exception e) {
            logger.error("Error dari mengocok arisan dalam grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------Payment Group Transaction-------------------------------------------------

    @RequestMapping(value = "/kocok-arisan-pay/{groupName}/{idTransaction}/{phoneNumber}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> walletBersamaPay(@PathVariable("phoneNumber") String phoneNumber, @PathVariable("groupName") String groupName, @PathVariable("idTransaction") long idTransaction) {
        logger.info("[Mentransfer uang pemenang arisan]");
        try {
            GroupTransaction groupTransactions = transactionService.arisanWinnerCheck(phoneNumber, groupName, idTransaction);
            if (groupTransactions == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Pembayaran gagal", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Pembayaran berhasil, silahkan cek saldo wallet grup", groupTransactions), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari membayar pemenang arisan: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------Show Group Wallet Admin-------------------------------------------------

    @RequestMapping(value = "/admin/getting-group-wallet/{groupName}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> showWalletBersamaAdmin(@PathVariable("groupName") String groupName) {
        logger.info("[Menampilkan saldo wallet grup]");

        try {
            GroupWallet groupWallets = transactionService.showSaldoGroupWalletAdmin(groupName);
            if (groupWallets == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Saldo wallet tidak tertampil", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan saldo wallet grup", groupWallets), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari menampilkan saldo wallet: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------Show Group Wallet-------------------------------------------------

    @RequestMapping(value = "/getting-group-wallet/{groupName}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> showWalletBersama(@PathVariable("groupName") String groupName) {
        logger.info("[Menampilkan saldo wallet grup oleh user]");

        try {
            GroupWallet groupWallets = transactionService.showSaldoGroupWallet(groupName);
            if (groupWallets == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Saldo wallet gagal ditampilkan", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan saldo wallet grup", groupWallets), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari menampilkan saldo wallet grup oleh user: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------Show Total Transaksi All-------------------------------------------------

    @RequestMapping(value = "/getting-transaction-value", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> showAllTotalTransaksi() {
        logger.info("[Menampilkan seluruh nilai transaksi dari aplikasi]");

        try {
            double totalTransaksi = transactionService.getAllTransactionValue();
            return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan nilai transaksi total aplikasi", totalTransaksi), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari menampilkan total transaksi user dalam aplikasi: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------Show Total Transaksi All-------------------------------------------------

    @RequestMapping(value = "/revenue", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> showRevenue() {
        logger.info("[Menampilkan revenue dari aplikasi hingga saat ini]");

        try {
            double revenue = transactionService.getRevenue();
            return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan nilai revenue aplikasi hingga saat ini", revenue), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari menampilkan revenue aplikasi: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }
}

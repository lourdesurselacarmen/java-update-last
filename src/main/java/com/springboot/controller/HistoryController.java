package com.springboot.controller;

import com.springboot.model.groupActivity.GroupTransaction;
import com.springboot.model.transaction.Transaction;
import com.springboot.response.ResponseHelper;
import com.springboot.response.ResponseHelperError;
import com.springboot.service.DataUserService;
import com.springboot.service.HistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/arisan-arisan-club")
@CrossOrigin(origins="*")
public class HistoryController {

    public static final Logger logger = LoggerFactory.getLogger(HistoryController.class);

    @Autowired
    private HistoryService historyService;
    @Autowired
    private DataUserService dataUserService;

    // ------------------------------------------History Transaksi User All-------------------------------------------------

    @RequestMapping(value = "/admin/history-transaksi/user", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> transactionHistoryUser() {
        logger.info("[Menampilkan history transaksi user dalam database]");

        try {
            List<Transaction> transactions = historyService.allHistoryTransactionUser();
            return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan seluruh data transaksi user", transactions), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari menampilkan transaksi: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------History Transaksi User by ID-------------------------------------------------

    @RequestMapping(value = "/history-transaksi/user", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> transactionHistoryUserbyID() {
        logger.info("[Menampilkan transaksi user saat ini dari database]");

        try {
            List<List<Transaction>> transactions = historyService.historyTransactionUser();
            return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan transaksi user saat ini", transactions), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari menampilkan transaksi user saat ini: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------History Transaksi User by ID Admin-------------------------------------------------

    @RequestMapping(value = "/admin/history-transaksi/userID/{username}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> transactionHistoryUserbyIDadmin(@PathVariable("username") String username) {
        logger.info("[Menampilkan transaksi user berdasarkan username]");

        try {
            List<List<Transaction>> transactions = historyService.historyTransactionUserAdmin(username);
            if (transactions == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Username belum terdaftar", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan transaksi user dengan username " + username, transactions), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari menampilkan transaksi user berdasarkan username: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------History Transaksi Group All-------------------------------------------------

    @RequestMapping(value = "/admin/history-transaksi/group", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> transactionHistoryGroup() {
        logger.info("[Menampilkan transaksi grup oleh admin]");

        try {
            List<GroupTransaction> groupTransactions = historyService.allHistoryTransactionGroup();
            return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan seluruh transaksi grup", groupTransactions), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari menampilkan seluruh transaksi grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------------History Transaksi Group by Group Name-------------------------------------------------

    @RequestMapping(value = "/history-transaksi/group/{groupName}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> transactionHistoryGroupByGroupName(@PathVariable("groupName") String groupName) {
        logger.info("[Menampilkan transaksi grup untuk grup " + groupName + "]");

        try {
            List<GroupTransaction> groupTransactions = historyService.historyTransactionGroup(groupName);
            if (groupTransactions == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Grup dengan nama grup ini tidak terdaftar", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menampilkan seluruh transaksi grup dalam grup " + groupName, groupTransactions), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari menampilkan transaksi grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }
}

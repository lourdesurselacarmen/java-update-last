package com.springboot.controller.auth;

import com.springboot.config.auth.JwtTokenUtil;
import com.springboot.model.auth.JwtRequest;
import com.springboot.model.auth.JwtResponse;
import com.springboot.dto.user.UserDTO;
import com.springboot.model.user.User;
import com.springboot.response.ResponseHelper;
import com.springboot.response.ResponseHelperError;
import com.springboot.service.JwtUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
public class JwtAuthenticationController {
    public static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @RequestMapping(value = "/arisan-arisan-club/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {
        logger.info("[User melakukan log in]");
        try {
            authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

            final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

            User user = userDetailsService.findByUsername(authenticationRequest.getUsername());
            boolean roleBool = user.isAdmin();
            String role = "";
            if (roleBool == false) {
                role = "user";
            } else if (roleBool == true) {
                role = "admin";
            }

            final String token = jwtTokenUtil.generateToken(userDetails);

            JwtResponse data =  new JwtResponse(userDetails.getUsername(), token, role);

            return new ResponseEntity<>(new ResponseHelper(true, "Anda berhasil log in", data), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari log in: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Silahkan cek kembali username dan password anda.", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/arisan-arisan-club/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody UserDTO user) {
        logger.info("[User melakukan registrasi]");
        try {

            if (userDetailsService.findUsername(user.getUsername())) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Username yang anda gunakan telah digunakan sebelumnya. Silahkan ubah username anda.", 400), HttpStatus.CONFLICT);
            } else if (userDetailsService.findPhone(user.getPhoneNumber())) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Nomor telepon yang anda gunakan telah terdaftar. Silahkan lakukan login dengan username yang bersangkutan.", 400), HttpStatus.CONFLICT);
            } else if (userDetailsService.findUsername(user.getUsername()) && userDetailsService.findPhone(user.getPhoneNumber())) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Nomor telepon dan username yang anda gunakan telah terdaftar. Silahkan lakukan login.", 400), HttpStatus.CONFLICT);
            } else {
                User users = userDetailsService.save(user);
                return new ResponseEntity<>(new ResponseHelper<>(true, "Akun anda telah didaftarkan", users), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari register: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage()+ " -- Silahkan cek kembali data anda.", 400), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/arisan-arisan-club/register-admin", method = RequestMethod.POST)
    public ResponseEntity<?> saveUserAdmin(@RequestBody UserDTO user) {
        logger.info("[Register untuk admin]");
        try {

            if (userDetailsService.findUsername(user.getUsername())) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Username yang anda gunakan telah digunakan sebelumnya. Silahkan ubah username anda.", 400), HttpStatus.CONFLICT);
            } else {
                User users = userDetailsService.saveAdmin(user);
                return new ResponseEntity<>(new ResponseHelper<>(true, "Akun anda telah didaftarkan", users), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari register admin: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage()+ " -- Silahkan cek kembali data anda.", 400), HttpStatus.BAD_REQUEST);
        }
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
package com.springboot.controller;

import com.springboot.dto.groupActivity.GroupMessageDTO;
import com.springboot.dto.groupMaking.GroupDTO;
import com.springboot.dto.groupMaking.MemberDTO;
import com.springboot.model.groupActivity.GroupMessage;
import com.springboot.model.groupMaking.Group;
import com.springboot.model.groupMaking.Member;
import com.springboot.response.ResponseHelper;
import com.springboot.response.ResponseHelperError;
import com.springboot.service.DataUserService;
import com.springboot.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/arisan-arisan-club")
@CrossOrigin(origins="*")
public class GroupController {
    public static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    private GroupService groupService;
    @Autowired
    private DataUserService dataUserService;

    // ------------------------------------Create a Group (User)------------------------------------------------

    @RequestMapping(value = "/create-group", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createGroup500(@RequestBody GroupDTO groupInfo) {
        logger.info("[Membuat grup baru dalam database]");

        try {
            Group groups = groupService.saveGroup(groupInfo);
            return new ResponseEntity<>(new ResponseHelper(true, "Membuat grup baru dengan info", groups), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari pembuatan grup baru: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Grup sudah ada, tidak dapat membuat grup", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Create a Group (Admin)------------------------------------------------

    @RequestMapping(value = "/admin/create-group/{money}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createGroupAdmin(@PathVariable("money") double money, @RequestBody GroupDTO groupInfo) {
        logger.info("[Membuat grup baru oleh admin]");

        try {
            Group groups = groupService.createGroup(groupInfo, money);
            return new ResponseEntity<>(new ResponseHelper(true, "Membuat grup baru dengan info", groups), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari pembuatan grup oleh admin: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Grup sudah ada, tidak dapat membuat grup", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Search a Group------------------------------------------------

    @RequestMapping(value = "/search-group/{groupName}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> searchGroup(@PathVariable("groupName") String groupName) {
        logger.info("[Mencari seluruh grup dalam database]");

        try {
            Group groups = groupService.findGroupName(groupName);
            return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan data grup dalam database", groups), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari pencarian seluruh grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Join a Group------------------------------------------------

    @RequestMapping(value = "/join-group/{groupName}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> joinGroup(@PathVariable("groupName") String groupName, @RequestBody GroupDTO groupJoin) {
        logger.info("[Masuk ke dalam grup dengan nama" + groupName + "]");

        try {
            Group groups = groupService.joinGroupMember(groupJoin, groupName);

            if(groups == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, " -- Kamu sudah menjadi member grup ini atau cek kembali passcode grup", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan informasi grup " + groupName, groups), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari join grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Change admin group------------------------------------------------

    @RequestMapping(value = "/change-admin-group/{groupName}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> changeAdminGroup(@PathVariable("groupName") String groupName, @RequestBody MemberDTO memberInfo) {
        logger.info("[Mengganti admin grup dengan nama "+ groupName + "]");

        try {
            Group groups = groupService.changeAdmin(groupName, memberInfo);

            if(groups == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, " -- Cek username member yang ingin dijadikan admin grup", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan informasi grup", groups), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari penggantian admin grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Change group info------------------------------------------------

    @RequestMapping(value = "/change-group-info/{groupName}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> changeGroupInfo(@PathVariable("groupName") String groupName, @RequestBody GroupDTO groupUpdate) {
        logger.info("[Mengganti info grup dalam database]");

        try {
            Group groups = groupService.changeGroupInfo(groupUpdate, groupName);

            if(groups == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, " -- Hanya admin yang dapat mengubah info grup", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan info grup", groups), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari penggantian info grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Search All Group------------------------------------------------

    @RequestMapping(value = "/search-group", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> searchAllGroup() {
        logger.info("[Mencari seluruh grup dalam database]");

        try {
            List<Group> groups = groupService.findAllGroup();
            return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan seluruh info grup", groups), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari pencarian seluruh grup dalam database: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Search Info Group yang Diikuti------------------------------------------------

    @RequestMapping(value = "/search-group-user", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> searchGroupUser() {
        logger.info("[Mencari info grup dalam database]");

        try {
            List<Member> members = groupService.findMemberbyUsername();
            List<Group> groups = groupService.listGroup(members);

            if(groups == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, " -- Username ini tidak memiliki list grup", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan list grup pada username bersangkutan", groups), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari pencarian list grup user saat ini: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Search Info Group yang Diikuti Admin------------------------------------------------

    @RequestMapping(value = "/admin/search-group-user/{username}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> searchGroupUserAdmin(@PathVariable("username") String username) {
        logger.info("[Mencari list grup user oleh admin]");

        try {
            List<Member> members = groupService.findMemberbyUsernameAdmin(username);
            List<Group> groups = groupService.listGroup(members);

            if(groups == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, " -- Username yang dicari tidak terdaftar", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan list grup dari username yang dicari", groups), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari pencarian list grup user: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Error has occured", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Cek Admin Grup------------------------------------------------

    @RequestMapping(value = "/cek-admin-grup/{groupName}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> checkAdmin(@PathVariable("groupName") String groupName) {
        logger.info("[Cek admin grup]");

        try {
            boolean memberAdmin = groupService.checkGroupAdmin(groupName);
            Member currentMember = groupService.findMemberbyUsernameInGroup(groupName);
            if(memberAdmin) {
                return new ResponseEntity<>(new ResponseHelper(true, "Member ini adalah seorang admin", currentMember), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ResponseHelperError(false, " -- Kamu bukan admin grup ini", 400), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            logger.error("Error dari pengecekan admin grup: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Search All Member in a Group------------------------------------------------

    @RequestMapping(value = "/search-group-member/{groupName}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> searchAllMemberGroup(@PathVariable("groupName") String groupName) {
        logger.info("[Mencari seluruh member dalam grup " + groupName + "]");

        try {
            List<Member> members = groupService.findMemberGroup(groupName);
            if (members == null) {
                return new ResponseEntity<>(new ResponseHelperError( false, "Tidak ada grup dengan nama " + groupName, 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan member grup " + groupName, members), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari pencarian member dalam grup " + groupName + ": " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Group Message Post------------------------------------------------

    @RequestMapping(value = "/post-message/group-board/{groupName}", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> postGroupMessage(@PathVariable("groupName") String groupName, @RequestBody GroupMessageDTO groupMessageFill) {
        logger.info("[Posting pesan dalam grup " + groupName + "]");

        try {
            List<GroupMessage> groupMessages = groupService.postMessage(groupName, groupMessageFill);
            if (groupMessages == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Gagal dalam mengirim pesan", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Pesan berhasil terkirim", groupMessages), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari posting pesan dalam grup " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------See All Post in Group------------------------------------------------

    @RequestMapping(value = "/group-board/{groupName}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> seeAllPostInGroup(@PathVariable("groupName") String groupName) {
        logger.info("[Melihat pesan dalam grup " + groupName + "]");

        try {
            List<GroupMessage> groupMessages = groupService.getMessages(groupName);
            if (groupMessages == null) {
                return new ResponseEntity<>(new ResponseHelperError( false, "Melihat pesan gagal", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Melihat seluruh pesan dalam grup", groupMessages), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari melihat pesan dalm grup " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Delete Group Message------------------------------------------------

    @RequestMapping(value = "/delete-message/group-board/{groupName}/{idMessage}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity<?> deleteGroupMessage(@PathVariable("groupName") String groupName, @PathVariable("idMessage") long idMessage) {
        logger.info("[Menghapus pesan dalam grup " + groupName + "]");

        try {
            List<GroupMessage> groupMessages = groupService.deleteMessage(groupName, idMessage);
            if (groupMessages == null) {
                return new ResponseEntity<>(new ResponseHelperError(false, "Penghapusan pesan gagal", 400), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(new ResponseHelper(true, "Pesan berhasil dihapus", groupMessages), HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Error dari menghapus pesan dalam grup " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }
}

package com.springboot.controller;

import com.springboot.response.ResponseHelper;
import com.springboot.response.ResponseHelperError;
import com.springboot.service.UploadFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/arisan-arisan-club")
public class UploadFileController {
    public static final Logger logger = LoggerFactory.getLogger(DataUserController.class);

    @Autowired
    private UploadFileService uploadFileService;

    // ------------------------------------Upload a File------------------------------------------------

    @GetMapping(value = "/files/{filename:.+}", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<?> getFile(@PathVariable String filename) {
        logger.info("[Getting a file in database]");

        try {
            Resource file = uploadFileService.load(filename);

            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment: filename=\"" + file.getFilename() + "\"").body(file);
        } catch (Exception e) {
            logger.error(String.valueOf(e));
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Error has occured", 400), HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/uploads")
    public ResponseEntity<ResponseHelper> uploadFile(@RequestParam("file")MultipartFile file) throws IllegalStateException, IOException {
        logger.info("[Uploading a file in database]");

        final String filename = uploadFileService.uploadFile(file);

        return new ResponseEntity<>(new ResponseHelper(true, "File has been successfully uploaded.", filename), HttpStatus.OK);
    }
}

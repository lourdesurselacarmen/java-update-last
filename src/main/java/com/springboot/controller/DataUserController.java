package com.springboot.controller;

import com.springboot.dto.user.UserDTO;
import com.springboot.model.user.User;
import com.springboot.response.ResponseHelper;
import com.springboot.response.ResponseHelperError;
import com.springboot.service.DataUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/arisan-arisan-club")
@CrossOrigin(origins="*")
public class DataUserController {
    public static final Logger logger = LoggerFactory.getLogger(DataUserController.class);

    @Autowired
    private DataUserService dataUserService;

    // ------------------------------------Know Current User------------------------------------------------

    @RequestMapping(value = "/current-user", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> currentUser() {
        logger.info("[Mencari data pengguna saat ini]");
        try {
            User currentUser = dataUserService.findCurrentUser();
            return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan pengguna saat ini", currentUser), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari pencarian data pengguna saat ini: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Retrieve All User Data------------------------------------------------

    @RequestMapping(value = "/userdata", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> listAllUser() {
        logger.info("[Mencari seluruh data pengguna]");

        try {
            List<User> users = dataUserService.findAllData();
            return new ResponseEntity<>(new ResponseHelper(true, "Menunjukkan seluruh data pengguna", users), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari pencarian data seluruh pengguna: " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Terjadi error", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // ------------------------------------Retrieve Single User Data by Username-----------------------------------------

    @RequestMapping(value = "/userdata/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserDatabyName(@PathVariable("username") String username) {
        logger.info("Mencari data pengguna dengan username: " + username);

        try {
            User users = dataUserService.findByUsername(username);
            return new ResponseEntity<>(new ResponseHelper(true, "Info untuk user dengan username" + username, users), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error dari pencarian data pengguna dengan username " + username + ": " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- User tidak terdaftar", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // --------------------------------------------Ganti Profil---------------------------------------------------

    @RequestMapping(value = "/userdata/ganti-profil", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> gantiProfil(@RequestBody UserDTO userUpdate) {
        logger.info("[Mengganti profil user saat ini]");

        try {
            User currentUser = dataUserService.findCurrentUser();

            logger.info("[Membuat data profil baru untuk pengguna saat ini]");
            dataUserService.changeProfile(userUpdate);

            User users = dataUserService.findByUserId(currentUser.getIdUser());

            return new ResponseEntity<>(new ResponseHelper(true, "Profil berhasil terganti", users), HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Error dari penggantian profil pengguna saat ini " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Data user tidak dapat ditemukan", 400), HttpStatus.BAD_REQUEST);
        }
    }

    // --------------------------------------------Ganti Profil Admin---------------------------------------------------

    @RequestMapping(value = "/userdata/ganti-profil-admin", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> gantiProfilAdmin(@RequestBody UserDTO userUpdate) {
        logger.info("[Mengganti profil admin saat ini]");

        try {
            User currentUser = dataUserService.findCurrentUser();

            logger.info("[Membuat data profil baru untuk admin saat ini]");
            dataUserService.changeProfileAdmin(userUpdate);

            User users = dataUserService.findByUserId(currentUser.getIdUser());

            return new ResponseEntity<>(new ResponseHelper(true, "Profil admin berhasil terganti", users), HttpStatus.CREATED);
        } catch (Exception e) {
            logger.error("Error dari penggantian profil admin saat ini " + e);
            return new ResponseEntity<>(new ResponseHelperError(false, e.getCause().getCause().getMessage() + " -- Data admin tidak dapat ditemukan", 400), HttpStatus.BAD_REQUEST);
        }
    }
}
